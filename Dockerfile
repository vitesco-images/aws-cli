FROM python:3.8-alpine

RUN apk add --no-cache \
      bash \
      git \
      groff \
      jq \
      less \
      mailcap \
      && \
    rm -rf /var/cache/apk/* /root/.cache/pip/*

RUN pip install --upgrade pip && \
    pip install --no-cache-dir \
      awscli

WORKDIR /root
VOLUME /root/.aws

ENTRYPOINT [ "aws" ]
